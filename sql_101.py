def create_database():
    '''
    CREATE DATABASE ebook_db;

    '''
    return None

def create_users_table():
    '''
    CREATE TABLE users_table(
    user_id INT AUTO_INCREMENT PRIMARY KEY ,
    first_name VARCHAR(60),
    last_name VARCHAR(60),
    email VARCHAR(100),
    phone_number INT
    );
    
    '''
    return "user table created"

def create_books_table():
    '''
    CREATE TABLE books_table (
    book_id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(200),
    description VARCHAR(500),
    date_published DATE ,
    price float
    );
    '''
    return "book table created"

def create_sales_table():
    '''
    CREATE TABLE sales_table(
    sale_id INT AUTO_INCREMENT PRIMARY KEY,
    sale_date DATE,
    user_id INT,
    book_id INT,
    FOREIGN KEY (user_id) REFERENCES users_table(user_id),
    FOREIGN KEY (book_id) REFERENCES books_table (book_id)
    );
    '''
    return "sales table created"

def add_column():
    
    '''
    ALTER TABLE books_table
    ADD user_id int,
    ADD FOREIGN KEY (user_id) REFERENCES users_table(user_id);
    '''

    '''
    ALTER TABLE books_table
    ADD user_id INT AUTO_INCREMENT PRIMARY KEY;
    '''
    return None

def alter_data_type():
    ''' 
    ALTER TABLE books_table
    MODIFY COLUMN price float;
    '''
    return None

def insert_new_book():
    '''
    INSERT books_table(title,description,date_published,price,user_id)
    VALUES('Joan Is Okay','Joan is a thirtysomething ICU doctor at a busy New York City hospital. ','2022-01-06', '12.50', '10');
    
    '''
    return None

def insert_new_user():
    '''
    INSERT users_table (first_name,last_name,email,phone_number)
    VALUES('big','mum','big@mum.com','07269384756');

    '''

    return "new user added"

def insert_sales_info():

    '''
    INSERT sales_table(sale_date,user_id,book_id)
    VALUES('2022-01-31','6','5');
    
    '''
    return None

def db_querys():

    # Q1: Get all the books
    '''
        SELECT * FROM books_table;
    '''

    # Q2: Get all the users
    '''
        SELECT * FROM users_table;
    '''

    # Q3: Get all the books, from user id 2
    '''
        SELECT * FROM books_table
        WHERE user_id = 2;
    '''

    # Q4: Get all the books, from user id 2 and 10
    '''
        SELECT * FROM books_table
        WHERE user_id = 2 AND user_id = 10;
    '''

    # Q5: Get all the books and data one the writers, from user id 2 and 4
    '''
        SELECT user_table.user_id
        FROM  
        INNER JOIN books_table ON 
        ON books_table.user_id;
    '''

    # Q6: Get me all the sales
    '''
        SELECT * FROM sales_table;
    '''

    # Q6: Get me all the sales and the emails of the buyers in one table
    '''
        SELECT sales_table.sale_id,sales_table.sale_date,sales_table.user_id,sales_table.book_id,users_table.email
        FROM sales_table
        LEFT JOIN users_table
        On sales_table.user_id = users_table.user_id;

    '''



    return None