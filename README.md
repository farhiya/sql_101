# SQL basics

This is a repo to introduce sql and DBs!

Objectives: 

- Intro to sql & db
- Intall mysql and workbench 
- define a table and columns

## Introduction to SQL and DB 

What is a Database? 

- a file with loads of information 
- Excel with contacts
- recipies in book? 
- bunch of postis with email and name? 

These can all be considered "databases" but really they are not very organise or easy to use. They have limitation. In tech these would not be what people are refering to as DBs. 

Usually in tech a DB is either:

- SQl or 
- NoSLQ (not only sql)

**SQL??** Structure Queery Language

... How is it structure? 

It has structure tables with connection between tables. Imagine excel sheets connect to one another. 

This makes it easy to query and get out the information we want :) 

#### Common Terminology 

**DB** Database
**SQL** Structure Queery Language how a DB might be organised
**DBMS** DataBase Management System

Inside a SQL DB, you will have 
**Columns** 
**rows**
**tables** 


### How do these connect


Books

| title                  | author_id | date | price | genre     |
|------------------------|-----------|------|-------|-----------|
| Harry Potter           | 1         | 2001 | 12    | fiction   |
| Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| Good Vibes - Good life | 2         | 2020 | 20    | Self Help |



Authors

| First_name | Last_name | email              |
|------------|-----------|--------------------|
| J. K.      | Rowling   | jk@gmail.com       |
| Vex        | King      | king@kingbooks.com |
| Harry      | Bookster  | hb@bookster.com    |


Genre

| type      | description                                 | other_info |
|-----------|---------------------------------------------|------------|
| fiction   | these are fictional noval                   | none       |
| Biography | some what true accounts of people's lives   | none       |
| Selfhelp  | Meant to help you think and better yourself | none       |


We can see how the above connect because we are machines that see context in everything.

But for a machine that is silicon based we need to use **Primary keys** and **Foreign keys**.

Primary keys are unique and cannot be reused.

Foreign keys reference a Primary key. They are essentially the primary key in a secondary table.




Let's see the above example with primary keys:

| book_id  | title                  | author_id | date | price | genre     |
|----------|------------------------|-----------|------|-------|-----------|
| 1        | Harry Potter           | 1         | 2001 | 12    | fiction   |
| 2        | Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| 3        | Good Vibes - Good life | 3         | 2020 | 20    | Self Help |


Authors

| author_id | First_name | Last_name | email              |
|-----------|------------|-----------|--------------------|
| 1         | J. K.      | Rowling   | jk@gmail.com       |
| 2         | Harry      | Bookster  | hb@bookster.com    |
| 3         | Vex        | King      | king@kingbooks.com |



### SQL Data Types

There are diferent types. Charindex, Charmax, decimals, integers. 

We'll look at the different!

excercise: Design a DB, define what is the best data type for each collum. 



### MYsql & Maria DB

Once you have install, enabled and started mysql or mariadb

```bash
mysql -u root -p -h localhost
```



### DDL Data Definition Languag

creating tables and columns

```sql 
-- Create a my_db
CREATE DATABESE my_db;

-- create a table
CREATE TABLE film_table (
    film_name VARCHAR(60);
    release_date DATE;
    decription VARCHAR(200)
);


```

In SQL we need to define the Datatypes for columns. Thefoolwing exit: 

- VARCHAR(n) - Adaptable to different character lengths
- VARCHAR(max) - Same as above with maximun lench
- CHARACTER or CHAR - Always takes that exact size
- Int - Integers - whole numbers
- DATE, TIME, DATETIME 
- DECIMAR or NUMERIC - Difines the number of digits to the right of the decimal. 
- BINARY - Used to story binary info such as pictures or music
- FLOAT - For very large numbers 
- BIT - similar to binary (0, 1, NULL)



##### Creating Tables

```sql
-- creating the film table
CREATE TABLE film_table ( filmID int NOT NULL, film_name VARCHAR(60), release_data DATE, description VARCHAR(200), PRIMARY KEY (filmIK) )


 desc film_table;
-- creating the director table
CREATE TABLE director_table ( 
    direcotID int NOT NULL, first_name VARCHAR(60), last_name VARCHAR(60) NOT NULL, email VARCHAR(120), PRIMARY KEY (directorIK))

```

##### Altering Tables


```SQL

-- add nationality to director table

-- add direcotID as a FOREIGN KEY in film_table

-- Alter films table so that title cannot be null

Alter table film_table MODIFY COLUMN film_name NOT NULL;
-- or
ALTER TABLE film_table CHANGE film_name film_name VARCHAR(60) NOT NULL;

-- Alter directors table to add a description to each director 

ALTER TABLE director_table ADD COLUMN description VARCHAR(200)

-- Alter table to remove description to each director

ALTER TABLE DROP COLUMN 

-- What is a dependent destroy constraint and how is it useful? 

```



### Data Manipulation Languae

Inserting, deling and altering row.

- INSERT INTO <TABLE> (dirctot_ID, First_name) VALUES ( 10 , 'Shannon')
- INSERT  <TABLE> VALUES ( 10 , 'Shannon', 'Lucas', 'sl@lucasfilms.com')
- DELETE FROM <table> WHERE COLUMN = <ID>
- UPDATE FROM <table> WHERE COLUMN = <ID>


### Data Query Language

Play around with SQL queering in the w3schools sql section.

```sql

-- normal Selector clause and From clause
SELECT <Column> FROM <Table>;

-- normal Selector clause and From clause + Where clause
SELECT <Column> FROM <Table> WHERE id=id;

-- Operators
SELECT <Column> FROM <Table> WHERE price = x
SELECT <Column> FROM <Table> WHERE price != x -- not equal
SELECT <Column> FROM <Table> WHERE price < x
SELECT <Column> FROM <Table> WHERE price > x
SELECT <Column> FROM <Table> WHERE price <= x
SELECT <Column> FROM <Table> WHERE price >= x

-- Logical and / or in Select 

SELECT <Column> FROM <Table> WHERE price = x AND categoryID =1 -- Both sides need to be true to get said value
SELECT <Column> FROM <Table> WHERE price = x or categoryID =1 -- Only one side need to be true to get value

-- Matcher & Wild cards %_[charlist][^charlist] LIKE
-- % is the wild card for any number or characters and any character 
SELECT <Column> FROM <Table> WHERE name LIKE 'j%';
SELECT <Column> FROM <Table> WHERE name LIKE '%SS';

-- _ is the substitute for one single character 
SELECT <Column> FROM <Table> WHERE name LIKE '____';

-- [charlist]% match 

-- [^charlist]% don't match


-- WHERE BETWEEN
SELECT * FROM [Products] WHERE PRICE BETWEEN 10 AND 22;

-- in ('list', 'items')
SELECT * FROM [Customers] WHERE country IN ('Germany', 'Mexico')


-- Alias - for columns and agregate data - Does not change original data
SELECT CustomerID AS ID, CustomerName AS 'Company Name', ContactName AS 'HUMAN TO INFLUENCE' FROM [Customers] WHERE country IN ('Germany', 'Mexico')

-- Alias the creation of new data
-- Figuring out how much to pay for 30 new of each product in category 1
SELECT *, price * 30 AS '$ to Order' FROM [Products] where categoryID = 1


-- Ordering our results
SELECT *, price * 30 AS '$ to Order' FROM [Products] where categoryID = 1 ORDER BY SupplierID DESC;

SELECT *, price * 30 AS '$ to Order' FROM [Products] where categoryID = 1 ORDER BY SupplierID ASC;

SELECT MIN(price) as 'Mininum price', MAX(price) as 'Max', AVG(price), SUM(price), COUNT(price) FROM [Products]

-- Aggregate functions with group by
SELECT SupplierID, MIN(price) as 'Mininum price', MAX(price) as 'Max', AVG(price), SUM(price), COUNT(price) FROM [Products] GROUP BY SupplierID


-- Aggregate functions whith group by and HAVING
SELECT SupplierID, AVG(price), COUNT(price) FROM [Products] GROUP BY SupplierID HAVING PRICE > 10


```



